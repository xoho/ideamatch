#!/bin/sh
echo "Start app"

case ${1} in
    start)
        cd /opt/app
        python3 manage.py makemigrations
        python3 manage.py migrate
        python3 manage.py collectstatic --no-input
        sleep 3
        gunicorn --access-logfile - --error-logfile - --workers 3 --bind 0.0.0.0:8000 ideamasher.wsgi:application
        while [ true ]; do sleep 1; done
        ;;
    *)
        exec ${@}
        ;;
esac
