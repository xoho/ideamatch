IMG = ideamasher:0.1
CONTAINER = ideamasher_app_1
IMGTAG = us.gcr.io/s3check-171914/${IMG}

build :
	docker build -t ${IMGTAG} .

rm :
	- docker rm --force ${CONTAINER}

run : build rm
	docker-compose up -d
	docker logs -f ${CONTAINER}

push : build
	gcloud docker -- push ${IMGTAG}
