FROM python:3.5-alpine

COPY requirements.txt /opt/app/
WORKDIR /opt/app
RUN pip3 install -r requirements.txt
COPY ./ /opt/app/
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD ["start"]
