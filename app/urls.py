""" app urls """
from django.conf.urls import url
from app.views import *


urlpatterns = [
    url(r'^search$', search, name="search"),
    url(r'^togglelike/(?P<idea_id>[0-9]+)$', togglelike, name="togglelike"),
    url(r'^toggleteam/(?P<idea_id>[0-9]+)/(?P<skill_id>[0-9]+)$', toggleteam, name="toggleteam"),
    url(r'^profile$', notimplemented, name="profile"),
    url(r'^messages$', notimplemented, name="messages"),
    url(r'^idea/(?P<pk>[0-9]+)$', idea, name="idea"),
    url(r'^idea$', notimplemented, name="create_idea"),
    url(r'^ideas/$', ideas, name="ideas"),
    url(r'^logout/$', app_logout, name="logout"),
    url(r'^login/$', app_login, name="login"),
    url(r'^signup/$', signup, name="signup"),
    url(r'^$', landing, name="landing"),
]
