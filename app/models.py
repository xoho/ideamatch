from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.

SKILLS = (
    ('project_management','Project Management'),
    ('finance','Finance'),
    ('web_development','Web Development'),
    ('app_development','App Development'),
    ('backend_development','Backend Development'),
    ('sales','Sales'),
    ('marketing','Marketing'),
    ('app_design','App Design'),
    ('web_design','Web Design'),
    ('branding','Branding'),
    ('industrial_design','Industrial Design'),
    ('making','Making'),
)

INTERESTS = (
    ('agriculture','Agriculture'),
    ('business','Business'),
    ('computer_and_electronics','Computer and Electronics'),
    ('consumer_services','Consumer Services'),
    ('education','Education'),
    ('energy_and_utilities','Energy and Utilities'),
    ('financial_services','Financial Services'),
    ('government','Government'),
    ('health_and_biotech','Health and Biotech'),
    ('manufacturing','Manufacturing'),
    ('media_and_entertainment','Media and Entertainment'),
    ('real_estate_and_construction','Real Estate and Construction'),
    ('retail','Retail'),
    ('social_action','Social Action'),
    ('software_and_internet','Software and Internet'),
    ('transportation_and_storage','Transportation and Storage'),
    ('travel','Travel'),
)  

class BaseModel(models.Model):
    """ base """
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

class Skill(BaseModel):
    """ skill """
    name = models.CharField(max_length=50)
    def __str__(self):
        return self.name

class Interest(BaseModel):
    """ Interest """
    name = models.CharField(max_length=50)
    def __str__(self):
        return self.name

class TeamMember(BaseModel):
    """ team member """
    user = models.ForeignKey(User, blank=True, null=True)
    team_skill = models.ForeignKey('Skill', blank=True)
    def __str__(self):
        return "%s - %s" % (self.user.username if self.user else "none", self.team_skill.name)

class Team(BaseModel):
    """ team """
    lead = models.ForeignKey(User, on_delete=models.CASCADE)
    members = models.ManyToManyField('TeamMember', blank=True)
    def __str__(self):
        return "%s" % (self.lead)


# Idea
class Idea(BaseModel):
    """ idea """
    title = models.CharField(max_length=256)
    description = models.CharField(max_length=2048)
    committment = models.IntegerField(default=0) # hours / week
    idea_team = models.ForeignKey('Team', blank=True, null=True)
    skills = models.ManyToManyField('Skill', blank=True)
    interests = models.ManyToManyField('Interest', blank=True)
    submitted_by = models.ForeignKey(User)
    def __str__(self):
        return self.title

    # def as_dict(self):
    #     data = dict()
    #     data['title'] = self.title
    #     data['description'] = self.description
    #     data['committment'] = self.committment
    #     data['skills'] = self.description
    #     data['interests'] = self.description
        

    
class Message(BaseModel):
    """ message """
    subject = models.CharField(max_length=256)
    body = models.CharField(max_length=2048)
    to_user = models.ForeignKey(User, related_name="to_user")
    from_user = models.ForeignKey(User, related_name="from_user")
    message_idea = models.ForeignKey("Idea", blank=True, related_name="message_idea")
    is_request_to_join = models.BooleanField(default=False)
    def __str__(self):
        return "%s - from: %s, to: %s" % (self.subject, self.from_user, self.to_user)


##
# Profile stuffs
##
class Profile(BaseModel):
    """ profile """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.CharField(max_length=2048)
    skills = models.ManyToManyField("Skill")
    interests  = models.ManyToManyField("Interest")
    idea_likes = models.ManyToManyField("Idea", blank=True)
    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()