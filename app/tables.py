""" tables """

from django.conf import settings
from django.utils.html import format_html
import django_tables2 as tables

class IdeasTable(tables.Table):
    """ ideas table """
    idea = tables.Column()

    def render_idea(self, idea):
        """ renders the idea """
        print(idea)
        return """
            <div class="row">
                <a href="/idea/{pk}" class="title">{title}</a>
            </div>
            <div class="row">
                <span>{description}</span>
            </div>
            <div class="row">
                {skills}
            </div>
            <div class="row" style="margin-top: 5px;">
                {interests}
            </div>
            """.format(
                pk=idea.pk,
                title=idea.title,
                description=idea.description,
                skills="".join(['<a class="btn flat-butt-xs flat-butt flat-info-butt">{}</a>'.format(x) for x in idea.skills.all()]),
                interests="".join(['<a class="btn flat-butt-xs flat-butt flat-info-butt">{}</a>'.format(x) for x in idea.interests.all()])
            )