# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-14 22:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='idea',
            name='interests',
        ),
        migrations.AddField(
            model_name='idea',
            name='interests',
            field=models.CharField(blank=True, choices=[('agriculture', 'Agriculture'), ('business', 'Business'), ('computer_and_electronics', 'Computer and Electronics'), ('consumer_services', 'Consumer Services'), ('education', 'Education'), ('energy_and_utilities', 'Energy and Utilities'), ('financial_services', 'Financial Services'), ('government', 'Government'), ('health_and_biotech', 'Health and Biotech'), ('manufacturing', 'Manufacturing'), ('media_and_entertainment', 'Media and Entertainment'), ('real_estate_and_construction', 'Real Estate and Construction'), ('retail', 'Retail'), ('social_action', 'Social Action'), ('software_and_internet', 'Software and Internet'), ('transportation_and_storage', 'Transportation and Storage'), ('travel', 'Travel')], max_length=50),
        ),
        migrations.RemoveField(
            model_name='idea',
            name='skills',
        ),
        migrations.AddField(
            model_name='idea',
            name='skills',
            field=models.CharField(blank=True, choices=[('project_management', 'Project Management'), ('finance', 'Finance'), ('web_development', 'Web Development'), ('app_development', 'App Development'), ('backend_development', 'Backend Development'), ('sales', 'Sales'), ('marketing', 'Marketing'), ('app_design', 'App Design'), ('web_design', 'Web Design'), ('branding', 'Branding'), ('industrial_design', 'Industrial Design'), ('making', 'Making')], max_length=50),
        ),
    ]
