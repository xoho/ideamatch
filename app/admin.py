from django.contrib import admin
from app.models import *
# Register your models here.

@admin.register(Skill)
class SkillAdmin(admin.ModelAdmin):
    """ admin """
    list_display = ('pk', 'name',)

@admin.register(Interest)
class InterestAdmin(admin.ModelAdmin):
    """ admin """
    list_display = ('pk', 'name',)

@admin.register(TeamMember)
class TeamMemberAdmin(admin.ModelAdmin):
    """ admin """
    list_display = ('pk', 'user', 'team_skill',)

@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    """ admin """
    list_display = ('pk', 'lead',)

@admin.register(Idea)
class IdeaAdmin(admin.ModelAdmin):
    """ admin """
    list_display = ('pk', 'title',)

@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    """ admin """
    #list_display = ('pk', 'subject', 'to_user', 'from_user',)

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    """ admin """
    list_display = ('user',)
