from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from django.contrib.auth.forms import AuthenticationForm
from app.models import Idea, Profile, Team, TeamMember, Skill
from quotes import Quotes

# Create your views here.
def landing(request):
    """ landing """
    return render(request, template_name="app/landing.html", context={})

def ideas(request):
    """ ideas """
    context = {}
    if request.GET.get('mine', None):
        print("filter on %s" % request.user)
        context['ideas'] = Idea.objects.filter(submitted_by=request.user)
    else:
        context['ideas'] = Idea.objects.all()
    context['likes'] = [x.pk for x in Profile.objects.get(user=request.user).idea_likes.all()]

    return render(request, template_name="app/home.html", context=context)

def app_login(request):
    """ login """
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect(reverse('ideas'))
        else:
            return redirect(reverse('login'))
    form = AuthenticationForm()
    return render(request, template_name="app/login.html", context={"form": form})

def app_logout(request):
    """ logout """
    logout(request)
    return redirect(reverse("landing"))

def create_idea(request):
    """ create new idea """
    return render(request, template_name="app/idea.html", context={})

def idea(request, pk):
    """ idea """
    _idea = Idea.objects.get(pk=pk)
    context = {}
    if _idea:
        context['idea'] = _idea
    else:
        return redirect(reverse('create_idea'))
    context['slots'] = [{"skill": str(x), "skill_id": x.pk} for x in _idea.skills.all()]
    members = _idea.idea_team.members.all()
    for slot in context['slots']:
        for member in members:
            print("member: %s - %s - %s" % (member.user.username, member.team_skill, slot['skill']))
            if slot['skill'] == str(member.team_skill):
                slot['staffedby'] = member.user.username
                break


    return render(request, template_name="app/idea.html", context=context)

def toggleteam(request, idea_id, skill_id):
    """ toggles membership on a team """
    _idea = Idea.objects.get(pk=idea_id)
    if _idea:
        team = _idea.idea_team
        member = team.members.filter(user=request.user, team_skill_id=skill_id)
        if member:
            print("removing member")
            print(member[0])
            team.members.remove(member[0])
        else:
            skill = Skill.objects.get(pk=skill_id)
            if skill:
                _member = TeamMember(user=request.user, team_skill=skill)
                _member.save()
                team.members.add(_member)

    return redirect(reverse('idea', args=(idea_id,)))

def notimplemented(request):
    """ not implemented landing page """
    quote = list(Quotes().random())
    while "internet" in str(quote).lower():
        quote = list(Quotes().random())

    context = {}
    if len(quote) > 1:
        context["quote"] = quote[1]
        context["by"] = quote[0]
    return render(request, template_name="app/notimplemented.html", context=context)

def togglelike(request, idea_id):
    """ toggles like on idea """
    _idea = Idea.objects.get(pk=idea_id)
    if _idea:
        profile = Profile.objects.get(user=request.user)
        if _idea in profile.idea_likes.all():
            profile.idea_likes.remove(_idea)
        else:
            profile.idea_likes.add(_idea)
    return redirect(reverse('ideas'))

def search(request):
    """ search """
    if request.method == "POST":
        print('searching...')
        # perform search
        print(request.POST.keys())
        try:
            terms = request.POST.get("search").replace(" ", ",").split(",")
        except:
            terms = []

        context = {}
        _ideas = []
        for term in terms:
            _ideas.extend([x.pk for x in Idea.objects.filter(title__contains=term)])

        context['ideas'] = [Idea.objects.get(pk=x) for x in list(set(_ideas))]
        context['likes'] = [x.pk for x in Profile.objects.get(user=request.user).idea_likes.all()]

        return render(request, template_name="app/home.html", context=context)


    return redirect(reverse('ideas'))

def signup(request):
    """ signup page """
    return render(request, template_name="app/signup.html", context={})